using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Data.SqlClient;
public class DAL
{
	public static DataTable ExecuteDataTable(string cns, string sqlStatement, params SqlParameter[] arrParam)
	{
		DataTable dt = default(DataTable);

		// Open the connection 
		using (SqlConnection cnn = new SqlConnection(cns)) {
			cnn.Open();

			// Define the command 
			using (SqlCommand cmd = new SqlCommand()) {
				cmd.Connection = cnn;
				cmd.CommandType = CommandType.Text;
				cmd.CommandText = sqlStatement;

				// Handle the parameters 
				if (arrParam != null) {
					foreach (SqlParameter param in arrParam) {
						cmd.Parameters.Add(param);
					}
				}

				// Define the data adapter and fill the dataset 
				using (SqlDataAdapter da = new SqlDataAdapter(cmd)) {
					dt = new DataTable();
					da.Fill(dt);

				}
			}
		}
		return dt;
	}
	public static object ExecuteSQL(string cns, string sqlStatement)
	{
		// Open the connection
		using (SqlConnection cnn = new SqlConnection(cns)) {
			cnn.Open();
			// Define the command 
			using (SqlCommand cmd = new SqlCommand()) {
				cmd.Connection = cnn;
				cmd.CommandType = CommandType.Text;
				cmd.CommandText = sqlStatement;
				cmd.ExecuteNonQuery();
			}
		}
        return "Complete";
	}

	public static DataTable ExecuteDataTableSP(string cns, string storedProcedureName, params SqlParameter[] arrParam)
	{
		DataTable dt = default(DataTable);
		DataSet ds = new DataSet();
		// Open the connection 
		using (SqlConnection cn = new SqlConnection(cns)) {
			cn.Open();

			// Define the command 
			using (SqlCommand cmd = new SqlCommand()) {
				cmd.Connection = cn;
				cmd.CommandType = CommandType.StoredProcedure;
				cmd.CommandText = storedProcedureName;

				// Handle the parameters 
				if (arrParam != null) {
					foreach (SqlParameter param in arrParam) {
						cmd.Parameters.Add(param);
					}
				}

				// Define the data adapter and fill the dataset 
				using (SqlDataAdapter da = new SqlDataAdapter(cmd)) {
					dt = new DataTable();
					da.Fill(dt);

				}
			}
		}

		return dt;
	}
}
