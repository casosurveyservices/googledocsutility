﻿namespace GoogleDocsUtility
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView_docView = new System.Windows.Forms.DataGridView();
            this.button_updateTable = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox_url = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label_sheetID = new System.Windows.Forms.Label();
            this.dataGridView_sheets = new System.Windows.Forms.DataGridView();
            this.button_UpdateSheets = new System.Windows.Forms.Button();
            this.textBox_range1 = new System.Windows.Forms.TextBox();
            this.textBox_range2 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label_selectedSheet = new System.Windows.Forms.Label();
            this.btnImportToDB = new System.Windows.Forms.Button();
            this.cbIncludeFileName = new System.Windows.Forms.CheckBox();
            this.cbCreateIdentity = new System.Windows.Forms.CheckBox();
            this.cbCreateTable = new System.Windows.Forms.CheckBox();
            this.cbDataStructure = new System.Windows.Forms.CheckBox();
            this.TextBox_TableName = new System.Windows.Forms.TextBox();
            this.lbl_recknt = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_docView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_sheets)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView_docView
            // 
            this.dataGridView_docView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_docView.Location = new System.Drawing.Point(12, 108);
            this.dataGridView_docView.Name = "dataGridView_docView";
            this.dataGridView_docView.Size = new System.Drawing.Size(919, 223);
            this.dataGridView_docView.TabIndex = 0;
            // 
            // button_updateTable
            // 
            this.button_updateTable.Location = new System.Drawing.Point(12, 13);
            this.button_updateTable.Name = "button_updateTable";
            this.button_updateTable.Size = new System.Drawing.Size(75, 23);
            this.button_updateTable.TabIndex = 1;
            this.button_updateTable.Text = "Update";
            this.button_updateTable.UseVisualStyleBackColor = true;
            this.button_updateTable.Click += new System.EventHandler(this.button_updateTable_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(94, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Google Sheet URL:";
            // 
            // textBox_url
            // 
            this.textBox_url.Location = new System.Drawing.Point(197, 13);
            this.textBox_url.Name = "textBox_url";
            this.textBox_url.Size = new System.Drawing.Size(734, 20);
            this.textBox_url.TabIndex = 3;
            this.textBox_url.Text = "https://docs.google.com/spreadsheets/d/15-_RB0VCcv7IZs9fKamthSWSiMXOj-kjGmHC4W4Qp" +
    "3Q/edit#gid=89725639";
            this.textBox_url.TextChanged += new System.EventHandler(this.textBox_url_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(94, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(104, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Interpreted sheet ID:";
            // 
            // label_sheetID
            // 
            this.label_sheetID.AutoSize = true;
            this.label_sheetID.Location = new System.Drawing.Point(194, 48);
            this.label_sheetID.Name = "label_sheetID";
            this.label_sheetID.Size = new System.Drawing.Size(0, 13);
            this.label_sheetID.TabIndex = 2;
            // 
            // dataGridView_sheets
            // 
            this.dataGridView_sheets.AllowUserToAddRows = false;
            this.dataGridView_sheets.AllowUserToDeleteRows = false;
            this.dataGridView_sheets.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_sheets.Location = new System.Drawing.Point(13, 373);
            this.dataGridView_sheets.MultiSelect = false;
            this.dataGridView_sheets.Name = "dataGridView_sheets";
            this.dataGridView_sheets.ReadOnly = true;
            this.dataGridView_sheets.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView_sheets.Size = new System.Drawing.Size(603, 226);
            this.dataGridView_sheets.TabIndex = 4;
            this.dataGridView_sheets.SelectionChanged += new System.EventHandler(this.dataGridView_sheets_SelectionChanged);
            // 
            // button_UpdateSheets
            // 
            this.button_UpdateSheets.Location = new System.Drawing.Point(12, 338);
            this.button_UpdateSheets.Name = "button_UpdateSheets";
            this.button_UpdateSheets.Size = new System.Drawing.Size(75, 23);
            this.button_UpdateSheets.TabIndex = 5;
            this.button_UpdateSheets.Text = "Update Sheets";
            this.button_UpdateSheets.UseVisualStyleBackColor = true;
            this.button_UpdateSheets.Click += new System.EventHandler(this.button_UpdateSheets_Click);
            // 
            // textBox_range1
            // 
            this.textBox_range1.Location = new System.Drawing.Point(197, 76);
            this.textBox_range1.Name = "textBox_range1";
            this.textBox_range1.Size = new System.Drawing.Size(100, 20);
            this.textBox_range1.TabIndex = 6;
            this.textBox_range1.Text = "A1";
            // 
            // textBox_range2
            // 
            this.textBox_range2.Location = new System.Drawing.Point(316, 76);
            this.textBox_range2.Name = "textBox_range2";
            this.textBox_range2.Size = new System.Drawing.Size(100, 20);
            this.textBox_range2.TabIndex = 6;
            this.textBox_range2.Text = "Z1000";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(94, 79);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(42, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Range:";
            // 
            // label_selectedSheet
            // 
            this.label_selectedSheet.AutoSize = true;
            this.label_selectedSheet.Location = new System.Drawing.Point(94, 343);
            this.label_selectedSheet.Name = "label_selectedSheet";
            this.label_selectedSheet.Size = new System.Drawing.Size(83, 13);
            this.label_selectedSheet.TabIndex = 7;
            this.label_selectedSheet.Text = "Selected Sheet:";
            // 
            // btnImportToDB
            // 
            this.btnImportToDB.Location = new System.Drawing.Point(663, 343);
            this.btnImportToDB.Name = "btnImportToDB";
            this.btnImportToDB.Size = new System.Drawing.Size(75, 23);
            this.btnImportToDB.TabIndex = 8;
            this.btnImportToDB.Text = "Import to DB";
            this.btnImportToDB.UseVisualStyleBackColor = true;
            this.btnImportToDB.Click += new System.EventHandler(this.btnImportToDB_Click);
            // 
            // cbIncludeFileName
            // 
            this.cbIncludeFileName.AutoSize = true;
            this.cbIncludeFileName.Checked = true;
            this.cbIncludeFileName.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbIncludeFileName.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbIncludeFileName.Location = new System.Drawing.Point(632, 442);
            this.cbIncludeFileName.Name = "cbIncludeFileName";
            this.cbIncludeFileName.Size = new System.Drawing.Size(128, 17);
            this.cbIncludeFileName.TabIndex = 57;
            this.cbIncludeFileName.Text = "Create LoadFileName";
            this.cbIncludeFileName.UseVisualStyleBackColor = true;
            // 
            // cbCreateIdentity
            // 
            this.cbCreateIdentity.AutoSize = true;
            this.cbCreateIdentity.Checked = true;
            this.cbCreateIdentity.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbCreateIdentity.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbCreateIdentity.Location = new System.Drawing.Point(632, 419);
            this.cbCreateIdentity.Name = "cbCreateIdentity";
            this.cbCreateIdentity.Size = new System.Drawing.Size(95, 17);
            this.cbCreateIdentity.TabIndex = 56;
            this.cbCreateIdentity.Text = "Create LoadID";
            this.cbCreateIdentity.UseVisualStyleBackColor = true;
            // 
            // cbCreateTable
            // 
            this.cbCreateTable.AutoSize = true;
            this.cbCreateTable.Checked = true;
            this.cbCreateTable.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbCreateTable.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbCreateTable.Location = new System.Drawing.Point(632, 373);
            this.cbCreateTable.Name = "cbCreateTable";
            this.cbCreateTable.Size = new System.Drawing.Size(115, 17);
            this.cbCreateTable.TabIndex = 55;
            this.cbCreateTable.Text = "Drop/Create Table";
            this.cbCreateTable.UseVisualStyleBackColor = true;
            // 
            // cbDataStructure
            // 
            this.cbDataStructure.AutoSize = true;
            this.cbDataStructure.Checked = true;
            this.cbDataStructure.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbDataStructure.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbDataStructure.Location = new System.Drawing.Point(632, 396);
            this.cbDataStructure.Name = "cbDataStructure";
            this.cbDataStructure.Size = new System.Drawing.Size(195, 17);
            this.cbDataStructure.TabIndex = 54;
            this.cbDataStructure.Text = "Determine Data Structure from Data";
            this.cbDataStructure.UseVisualStyleBackColor = true;
            // 
            // TextBox_TableName
            // 
            this.TextBox_TableName.Location = new System.Drawing.Point(632, 466);
            this.TextBox_TableName.Name = "TextBox_TableName";
            this.TextBox_TableName.Size = new System.Drawing.Size(270, 20);
            this.TextBox_TableName.TabIndex = 58;
            this.TextBox_TableName.Text = "TestTable";
            // 
            // lbl_recknt
            // 
            this.lbl_recknt.AutoSize = true;
            this.lbl_recknt.Location = new System.Drawing.Point(632, 493);
            this.lbl_recknt.Name = "lbl_recknt";
            this.lbl_recknt.Size = new System.Drawing.Size(53, 13);
            this.lbl_recknt.TabIndex = 59;
            this.lbl_recknt.Text = "lbl_recknt";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(943, 664);
            this.Controls.Add(this.lbl_recknt);
            this.Controls.Add(this.TextBox_TableName);
            this.Controls.Add(this.cbIncludeFileName);
            this.Controls.Add(this.cbCreateIdentity);
            this.Controls.Add(this.cbCreateTable);
            this.Controls.Add(this.cbDataStructure);
            this.Controls.Add(this.btnImportToDB);
            this.Controls.Add(this.label_selectedSheet);
            this.Controls.Add(this.textBox_range2);
            this.Controls.Add(this.textBox_range1);
            this.Controls.Add(this.button_UpdateSheets);
            this.Controls.Add(this.dataGridView_sheets);
            this.Controls.Add(this.textBox_url);
            this.Controls.Add(this.label_sheetID);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button_updateTable);
            this.Controls.Add(this.dataGridView_docView);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_docView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_sheets)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView_docView;
        private System.Windows.Forms.Button button_updateTable;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox_url;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label_sheetID;
        private System.Windows.Forms.DataGridView dataGridView_sheets;
        private System.Windows.Forms.Button button_UpdateSheets;
        private System.Windows.Forms.TextBox textBox_range1;
        private System.Windows.Forms.TextBox textBox_range2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label_selectedSheet;
        private System.Windows.Forms.Button btnImportToDB;
        internal System.Windows.Forms.CheckBox cbIncludeFileName;
        internal System.Windows.Forms.CheckBox cbCreateIdentity;
        internal System.Windows.Forms.CheckBox cbCreateTable;
        internal System.Windows.Forms.CheckBox cbDataStructure;
        private System.Windows.Forms.TextBox TextBox_TableName;
        private System.Windows.Forms.Label lbl_recknt;
    }
}

