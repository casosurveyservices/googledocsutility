﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Google.GData.Client;
using Google.GData.Extensions;
using Google.GData.Spreadsheets;
using System.Runtime.InteropServices;
using Google.Apis.Auth;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Sheets.v4;
using Google.Apis.Sheets.v4.Data;
using Google.Apis.Services;
using Google.Apis.Util.Store;
using System.IO;
using System.Threading;
using System.Security.Cryptography.X509Certificates;

/*
* Google Project ID: thinking-creek-135819 
* client ID: 702638285437-tqlcj5vk6b373ich34ctjjt9r7ihfp6m.apps.googleusercontent.com
* client secret: BWrCJtFUBdB-Ia1EJhG6FJxH
*/

namespace GoogleDocsUtility
{
    public partial class Form1 : Form
    {

        // If modifying these scopes, delete your previously saved credentials
        // at ~/.credentials/sheets.googleapis.com-dotnet-quickstart.json
        string pub_connectionstring = "Persist Security Info=True;User ID=remote_timwil;Initial Catalog=Admin;Data Source=casoaz-prod-sql.cloudapp.net; Password=graph130;";
        static string[] Scopes = { SheetsService.Scope.SpreadsheetsReadonly };
        static string ApplicationName = "Google Docs Utility";

        [DllImport("kernel32.dll", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool AllocConsole();

        public Form1()
        {
            InitializeComponent();
            AllocConsole();
            UpdateSheetIDText();
            //Tabs();
        }

        public void Tabs()
        {
            //https://www.googleapis.com/oauth2/v1/certs","client_secret":"BWrCJtFUBdB-Ia1EJhG6FJxH"

            var certificate = new X509Certificate2(@"./GoogleDocUtility-039da8843843.p12", "notasecret", X509KeyStorageFlags.Exportable);
            //var certificate = new X509Certificate2(@"client_secret.json");//, "BWrCJtFUBdB-Ia1EJhG6FJxH");

            const string user = "702638285437-compute@developer.gserviceaccount.com";

            var serviceAccountCredentialInitializer = new ServiceAccountCredential.Initializer(user)
            {
                Scopes = new[] { "https://spreadsheets.google.com/feeds" }
            }.FromCertificate(certificate);

            var credential = new ServiceAccountCredential(serviceAccountCredentialInitializer);

            if (!credential.RequestAccessTokenAsync(System.Threading.CancellationToken.None).Result)
                throw new InvalidOperationException("Access token request failed.");

            var requestFactory = new GDataRequestFactory(null);
            requestFactory.CustomHeaders.Add("Authorization: Bearer " + credential.Token.AccessToken);

            var service = new SpreadsheetsService(null) { RequestFactory = requestFactory };

            // Instantiate a SpreadsheetQuery object to retrieve spreadsheets.
            SpreadsheetQuery query = new SpreadsheetQuery();            

            // Make a request to the API and get all spreadsheets.
            SpreadsheetFeed feed = service.Query(query);
            
            if (feed.Entries.Count == 0)
            {
                Console.WriteLine("There are no sheets");
            }

            // Iterate through all of the spreadsheets returned
            foreach (SpreadsheetEntry sheet in feed.Entries)
            {
                Console.WriteLine("******************************************************************************************");
                // Print the title of this spreadsheet to the screen
                Console.WriteLine(sheet.Title.Text);

                // Make a request to the API to fetch information about all
                // worksheets in the spreadsheet.
                WorksheetFeed wsFeed = sheet.Worksheets;

                // Iterate through each worksheet in the spreadsheet.
                foreach (WorksheetEntry entry in wsFeed.Entries)
                {
                    Console.WriteLine("------------------------------------------------------------------------------------");
                    // Get the worksheet's title, row count, and column count.
                    string title = entry.Title.Text;
                    var rowCount = entry.Rows;
                    var colCount = entry.Cols;

                    DataTable dt = new DataTable();
                    for (int colCnt = 0; colCnt<colCount; colCnt++) dt.Columns.Add();
                    // Print the fetched information to the screen for this worksheet.
                    Console.WriteLine(title + "- rows:" + rowCount + " cols: " + colCount);

                    DataRow dhr = dt.NewRow();
                    dhr.BeginEdit();
                    Console.WriteLine(dhr.ItemArray.Length); 

                    CellQuery cellQuery = new CellQuery(entry.CellFeedLink);

                    CellFeed cellFeed = service.Query(cellQuery);

                    if (cellFeed.Entries.Count > 0)
                    {
                        //create column header row
                        for (int c = 0; c < colCount; c++)
                        {
                            CellEntry cell = cellFeed.Entries[c] as CellEntry;
                            dhr[c] = cell;
                            Console.WriteLine(cell.Value);
                        }
                    }
                    /*
                    DataRow dr = dt.NewRow();
                    // Iterate through each cell, updating its value if necessary.
                    
                    foreach (CellEntry cell in cellFeed.Entries)
                    {
                        DataGridCell dgc = new DataGridCell((int)cell.Row, (int)cell.Column);

                        if (cell.Row > dt.Rows.Count)
                        {
                            dr = dt.NewRow();
                            dt.Rows.Add(dr);
                        }
                        dr[(int)cell.Column] = cell.Value;

                        Console.WriteLine(cell.Value);
                    }

                    dataGridView_sheets.DataSource = dt;
                    */
                    // Create a local representation of the new worksheet.
                    //WorksheetEntry worksheet = new WorksheetEntry();
                    //worksheet.Title.Text = "New Worksheet";
                    //worksheet.Cols = 10;
                    //worksheet.Rows = 20;

                    // Send the local representation of the worksheet to the API for
                    // creation.  The URL to use here is the worksheet feed URL of our
                    // spreadsheet.
                    //WorksheetFeed NewwsFeed = sheet.Worksheets;
                    //service.Insert(NewwsFeed, worksheet);
                }

                //dataGridView_sheets.DataSource = wsFeed.Entries[0].Source;

                /*
                IList<IList<Object>> values = (wsFeed.Entries[0] as WorksheetEntry);
                if (values != null && values.Count > 0)
                {
                    int colCount = 0;
                    string rowText = "";
                    DataTable dt = new DataTable();
                    foreach (var col in values[0])
                    {
                        rowText += col + ", ";
                        colCount++;
                        DataColumn dc = new DataColumn(col.ToString());

                        //check for duplicate column name
                        if (dt.Columns.Contains(dc.ToString()))
                            dt.Columns.Add(new DataColumn(col.ToString() + col.ToString()[col.ToString().Length - 1]));
                        else
                            dt.Columns.Add(dc);
                    }
                    Console.WriteLine(colCount);
                    for (var r = 1; r < values.Count; r++)
                    {
                        DataRow dr = dt.NewRow();
                        dr.ItemArray = values[r].ToArray();
                        dt.Rows.Add(dr);
                    }
                    dataGridView_docView.DataSource = dt;
                }
                else
                {
                    Console.WriteLine("No data found.");
                }
                */
            }
        }

        public void SpreadSheetTest()
        {

            UserCredential credential;

            using (var stream =
                new FileStream("client_secret.json", FileMode.Open, FileAccess.Read))
            {
                string credPath = System.Environment.GetFolderPath(
                    System.Environment.SpecialFolder.Personal);
                credPath = Path.Combine(credPath, ".credentials/sheets.googleapis.com-dotnet-quickstart.json");

                credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
                    GoogleClientSecrets.Load(stream).Secrets,
                    Scopes,
                    "user",
                    CancellationToken.None,
                    new FileDataStore(credPath, true)).Result;
                Console.WriteLine("Credential file saved to: " + credPath);
            }

            // Create Google Sheets API service.
            var service = new SheetsService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = ApplicationName,
            });

            // Define request parameters. 
            //String spreadsheetId = "15-_RB0VCcv7IZs9fKamthSWSiMXOj-kjGmHC4W4Qp3Q";
            String spreadsheetId = label_sheetID.Text;
            String range = textBox_range1.Text + ":" + textBox_range2.Text;
            SpreadsheetsResource.ValuesResource.GetRequest request =
                    service.Spreadsheets.Values.Get(spreadsheetId, range);

            ValueRange response = request.Execute();
            IList<IList<Object>> values = response.Values;
            if (values != null && values.Count > 0)
            {
                int colCount = 0;
                string rowText = "";
                DataTable dt = new DataTable();
                foreach (var col in values[0])
                {
                    rowText += col + ", ";
                    colCount++;
                    DataColumn dc = new DataColumn(col.ToString());

                    //check for duplicate column name
                    if (dt.Columns.Contains(dc.ToString()))
                        dt.Columns.Add(new DataColumn(col.ToString()+col.ToString()[col.ToString().Length-1]));
                    else
                        dt.Columns.Add(dc);
                }
                Console.WriteLine(colCount);
                for ( var r = 1; r < values.Count; r++ )
                {
                    DataRow dr = dt.NewRow();                    
                    dr.ItemArray = values[r].ToArray();
                    dt.Rows.Add(dr);
                }
                dataGridView_docView.DataSource = dt;
            }
            else
            {
                Console.WriteLine("No data found.");
            }
        }

        public void GetSheets()
        {

            UserCredential credential;

            using (var stream =
                new FileStream("client_secret.json", FileMode.Open, FileAccess.Read))
            {
                string credPath = System.Environment.GetFolderPath(
                    System.Environment.SpecialFolder.Personal);
                credPath = Path.Combine(credPath, ".credentials/sheets.googleapis.com-dotnet-quickstart.json");

                credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
                    GoogleClientSecrets.Load(stream).Secrets,
                    Scopes,
                    "user",
                    CancellationToken.None,
                    new FileDataStore(credPath, true)).Result;
                Console.WriteLine("Credential file saved to: " + credPath);
            }

            // Create Google Sheets API service.
            var service = new SheetsService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = ApplicationName,
            });
            
            String spreadsheetId = label_sheetID.Text;
            SpreadsheetsResource.GetRequest request =
                    service.Spreadsheets.Get(spreadsheetId);
            Spreadsheet response = request.Execute();
            IList<Sheet> values = response.Sheets;

            if (values != null && values.Count > 0)
            {
                DataTable dt = new DataTable();
                DataColumn dc1 = new DataColumn("Num");
                DataColumn dc2 = new DataColumn("Sheet ID");
                DataColumn dc3 = new DataColumn("Sheet Name");
                dt.Columns.Add(dc1);
                dt.Columns.Add(dc2);
                dt.Columns.Add(dc3);
                for (var r = 0; r < values.Count; r++)
                {
                    DataRow dr = dt.NewRow();
                    dr[0] = values[r].Properties.Index;
                    dr[1] = values[r].Properties.SheetId;
                    dr[2] = values[r].Properties.Title;
                    dt.Rows.Add(dr);
                }
                dataGridView_sheets.DataSource = dt;     
                //dataGridView_sheets.DataSource = values;
            }
            else
            {
                Console.WriteLine("No data found.");
            }
        }
        public void UpdateSheetIDText()
        {
            //label_sheetID.Text = textBox_url.Text.Substring(39, textBox_url.Text.LastIndexOf(@"/") - 39);
            label_sheetID.Text = textBox_url.Text.Substring(39, textBox_url.Text.LastIndexOf("/edit") -39);
        }

        private void button_updateTable_Click(object sender, EventArgs e)
        {
            SpreadSheetTest();
        }

        private void textBox_url_TextChanged(object sender, EventArgs e)
        {
            UpdateSheetIDText();
        }

        private void button_UpdateSheets_Click(object sender, EventArgs e)
        {
            GetSheets();
        }

        private void dataGridView_sheets_SelectionChanged(object sender, EventArgs e)
        {
            if (dataGridView_sheets.SelectedRows.Count > 0)
            {
                label_selectedSheet.Text = dataGridView_sheets.SelectedRows[0].Cells[2].Value.ToString();
                Console.WriteLine("Selected: " + dataGridView_sheets.SelectedRows[0].Cells[2].Value.ToString());
            }
        }

        private void btnImportToDB_Click(object sender, EventArgs e)
        {
            
            string strSQL = null;
            string strSQLd = null;
            string strSQLi = null;
            string strSQLa = "";
            string valuetest = null;
            Int16 knt = default(Int16);
            Int32 recknt = default(Int32);
            int datamaxlen = 0;
            int i = 0;
            this.Cursor = Cursors.WaitCursor;
            knt = 0;
            if (cbCreateIdentity.Checked) {
	            strSQL = "CREATE TABLE [" + TextBox_TableName.Text + "] ([LoadID] [int] IDENTITY (1, 1) NOT NULL ,";
            } else {
	            strSQL = "CREATE TABLE [" + TextBox_TableName.Text + "] (";
            }
            strSQLi = "insert into [" + TextBox_TableName.Text + "] (";
            foreach(DataGridViewColumn column in  dataGridView_docView.Columns) {
	            // Determine Length of Data in each column in order to make the database handle the proper length
	            datamaxlen = 100;
	            if (cbDataStructure.Checked ) {
		            lbl_recknt.Text = "Creating Structure from " + dataGridView_docView.Rows.Count + " records";
		            lbl_recknt.Refresh();
		            datamaxlen = 1;
		            for (i = 0; i <= dataGridView_docView.Rows.Count - 2; i++) {
                        if (dataGridView_docView.Rows[i].Cells[column.Index].Value.ToString().Length > datamaxlen) {
				            datamaxlen = dataGridView_docView.Rows[i].Cells[column.Index].Value.ToString().Length;
			            }
		            }
	            }
	            datamaxlen = datamaxlen + 5;
	            if (column.Name != "Comments" & column.Name != "detail" & column.Name != "description") {
		            strSQL = strSQL + "[" + column.Name + "] nvarchar(" + datamaxlen + ") NULL";
	            } else {
		            strSQL = strSQL + "[" + column.Name + "] ntext NULL";
	            }
	            strSQLi = strSQLi + "[" + column.Name + "] ";
	            knt += 1;
	            if (knt < dataGridView_docView.ColumnCount) {
		            strSQL = strSQL + ",";
		            strSQLi = strSQLi + ",";
	            }
            }
            if (cbIncludeFileName.Checked) {
	            strSQL = strSQL + ",LoadFileName nvarchar(100) NULL";
	            strSQLi = strSQLi + ",LoadFileName";
                strSQLa = "Autoload";
            }
            strSQL = strSQL + ")";
            strSQLi = strSQLi + ") ";
            // drop existing table with the same name

            if (cbCreateTable.Checked) {
	            try {
		            strSQLd = "drop table [" + TextBox_TableName.Text + "] ";
		            DAL.ExecuteSQL(pub_connectionstring, strSQLd);
	            } catch (Exception ex) {
		            if (ex.Message.Substring(0, 21) != "Cannot drop the table") {
			            MessageBox.Show(ex.Message);
		            }
	            }

	            //create table
	            try {
		            DAL.ExecuteSQL(pub_connectionstring, strSQL);
	            } catch (Exception ex) {
		            MessageBox.Show(ex.Message);
	            }
            }

            // Insert Records into created table
            try {
	            string strSQLinsert = null;
	            UnicodeEncoding uni = new UnicodeEncoding();
	            string fldstr = null;

	            strSQLinsert = "";
	            foreach (DataGridViewRow row in dataGridView_docView.Rows) {
		            recknt +=1;
		            if (recknt == dataGridView_docView.RowCount) {
			            break; // TODO: might not be correct. Was : Exit For
		            }
		            strSQLinsert = strSQLi;
		            strSQLinsert = strSQLinsert + " VALUES (";
		            knt = 1;
                    foreach (DataGridViewColumn column in dataGridView_docView.Columns) {
			            //column = column_loopVariable;
                        fldstr = row.Cells[column.Index].Value.ToString().Replace("'", "''");
			            if (fldstr == "NULL") {
				            strSQLinsert = strSQLinsert + "NULL";
			            } else {
				            strSQLinsert = strSQLinsert + "N'" + fldstr + "'";
			            }
			            //strSQLinsert = strSQLinsert & "N'" & fldstr & "'"
			            if (knt < dataGridView_docView.ColumnCount) {
				            strSQLinsert = strSQLinsert + ",";
			            }
			            knt += 1;
		            }
                    if (cbIncludeFileName.Checked) {
                        strSQLinsert = strSQLinsert + ",N'" + strSQLa + "'";
                    }

		            strSQLinsert = strSQLinsert + ")";
		            // db row insert
		            DAL.ExecuteSQL(pub_connectionstring, strSQLinsert);
		            if (recknt % 10 == 0) {
			            lbl_recknt.Text = "Loading Records: " + recknt;
			            //lbl_recknt.Refresh()
			            Application.DoEvents();
		            }
	            }
            } catch (Exception ex) {
	            MessageBox.Show("Insert Records: " + ex.Message);
            }
            lbl_recknt.Text = recknt.ToString();
            System.Windows.Forms.Application.DoEvents();
            this.Cursor = Cursors.Default;
        }
    }
}